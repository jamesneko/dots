package TestHelper;

use warnings;
use strict;
use v5.16;
use Exporter qw/import/;
use File::Temp;
use Test::More;
use File::Basename;
use Cwd qw/abs_path/;

our @EXPORT = qw/testdir run init_work_dir init_home_dir init_repo_dir/;


sub testdir
{
  return abs_path dirname $0;
}


sub run
{
  my (@cmd) = @_;
  diag "run(" . join(' ', @cmd) . ")";
  system(@cmd);
  my $ecode = $? >> 8;
  die "Running " . join(' ', @cmd) . " failed with code $ecode." if $ecode;
}


sub init_work_dir
{
  my $cleanup = 1;
  if ($ENV{DOTS_TEST_NO_CLEANUP}) {
    $cleanup = 0;
    diag "DOTS_TEST_NO_CLEANUP set, dir will not be removed.";
  }
  my $dir = File::Temp->newdir("/tmp/dots-test-$$-XXXX", CLEANUP => $cleanup);
  return $dir;
}


# must be run before init_repo_dir, as we need to set git defaults in this new test homedir
sub init_home_dir
{
  my ($copy_from, $init_as) = @_;
  run('cp', '-a', $copy_from, $init_as);
  $ENV{HOME} = $init_as;
  $ENV{XDG_CONFIG_HOME} = "$init_as/.config";
  diag "Set HOME to '$init_as'";
  run('git', 'config', '--global', 'user.email', 'dots-test@example.com');
  run('git', 'config', '--global', 'user.name', 'dots-test');
}


sub init_repo_dir
{
  my ($copy_from, $init_as) = @_;
  my $bare_git = "$init_as.git";
  die "init_home_dir must be used first!" unless $ENV{HOME} =~ m{^/tmp};
  
  # First need to init a bare repo
  run('git', '--bare', 'init', $bare_git);

  # Then check that out temporarily and add files to it.
  run('git', 'clone', $bare_git, $init_as);
  run('cp', '-a', "$copy_from/.", $init_as);
  run('git', '-C', $init_as, 'add', '.');
  run('git', '-C', $init_as, 'commit', '-m', "Setup of testing repo '$init_as' from '$copy_from'");
  run('git', '-C', $init_as, 'push');
  return $bare_git;
}



1;

