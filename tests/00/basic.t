use warnings;
use strict;
use v5.16;
use Test::More;

use TestHelper;

my $workdir = init_work_dir();
my $testdir = testdir();
diag 'DIR: ', $workdir;
diag 'CWD: ', `pwd`;
diag 'TESTS: ', $testdir;

init_home_dir("$testdir/initial", "$workdir/home");
my $repourl = init_repo_dir("$testdir/repo", "$workdir/repo");


plan tests => 2;

subtest 'initial state ok' => sub {
  plan tests => 4;
  ok(-f "$workdir/repo/.xscreensaver");
  ok(-d "$workdir/repo/.config");
  ok(-f "$workdir/home/.bash_profile");
  ok(-d "$workdir/home/.config");
};


subtest 'dots init' => sub {
  plan tests => 4;
  run('bin/dots', 'init', $repourl);

  ok(-d "$workdir/home/.dots", ".dots created");
  ok(-f "$workdir/home/.dots/config", "config created");
  ok(-f "$workdir/home/.dots/repo/.xscreensaver", "repo cloned");
  ok(-f "$workdir/home/.dots/repo/dots.config", "repo config created");
};
