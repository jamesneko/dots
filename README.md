# Dots

Dotfile management script that wraps a few git commands and maintains a bunch of symlinks into a `~/.dots/` dir.
